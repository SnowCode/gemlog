# An awesome email client for terminals
I first used thunderbrid to manage my emails, then I switched for a very short time to mutt before discovering aerc, which is a very good email client. This is how to install it:

## Step 1: Installing dependencies 
First we have to install golang. First, download the golang tarball:

=> https://golang.org/dl Downloads page of Go

Then run the following commands to install it on your system.

```
$ sudo mv Downloads/go*.tar.gz /usr/local
$ cd /usr/local
$ sudo tar xvf go*.tar.gz
$ sudo rm go*.tar.gz
$ sudo ln -s /usr/local/go/bin/* /usr/bin
```

Then we can install scdoc.

```
$ git clone https://git.sr.ht/~sircmpwn/scdoc
$ cd scdoc
$ make
$ sudo make install
```

## Step 2: Installing aerc
Now we can install aerc the same way we installed scdoc:

```
$ git clone https://git.sr.ht/~sircmpwn/aerc
$ cd aerc
$ make
$ sudo make install
```

## Step 3.1: Configuring gmail to work with aerc
Gmail doesn't trust a lot of clients. So we need to tell gmail aerc is trustworthy. To do that we need to go in the security settings of gmail:

=> https://myaccount.google.com/security Google's security settings

Over there, click to enable "2-step verification", then go back to the security page and click on "app passwords". 

In this new panel, select a new app of type "Other", and name it "aerc". Then you have to copy and save the given password, you won't be able to access it later on.

## Step 3.2: Configuring aerc
Now you simply have to run the setup of aerc:

```
$ aerc
```

Over there, simply answer all the questions, if you don't know the specific configurations, it's not a problem. You just have to tell your email and app password.

Once you did all of that, you can now use aerc. If you want to know the keyboard shortcuts and how to use aerc you can checkout its man page:

```
$ man aerc-tutorial
```

aerc also have other man pages, and you can also use :help inside of aerc to view the different aerc's commands available.

## Conclusion
The reason why I decided to use aerc instead of mutt and thunderbird:

* It's lighter than thunderbird
* Require less configuration than mutt
* Unlike mutt, it can display most of HTML emails

